/*globals module, require */

var numberConverter = (function () {
    "use strict";

    var _ = require('lodash');
    var messages = require('../resources/res_bundle');
    var helper = require('./number_converter_helper');

    function validateNumber(number) {
        if (!helper.isArgValid(number)) {
            console.log(messages.INVALID_ARGUMENT);
            throw new Error(messages.INVALID_ARGUMENT);
        }
    }

    function convertToWord(number) {

        validateNumber(number);

        var text = _.chain(number.toString())
            .toArray()
            .map(_.partial(parseInt, _, 10))
            .reverse()
            .chunk(3)
            .reverse()
            .map(function (i) {
                return i.reverse();
            })
            .map(helper.buildHundreds)
            .map(helper.addPostfix)
            .join(' ')
            .trim()
            .value();

        return text;
    }

    return {
        convertToWord: convertToWord
    };

})();

module.exports = numberConverter;
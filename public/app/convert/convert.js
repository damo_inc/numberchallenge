'use strict';

angular.module('myApp.convert', ['ngRoute', 'ngResource'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/convert', {
            templateUrl: 'convert/convert.html',
            controller: 'ConvertCtrl'
        });
    }])
    .controller('ConvertCtrl', ['$scope', '$routeParams', '$http', function ($scope, $routeParams, $http) {

        $scope.submit = function () {
            $http.get('/conversion/' + $scope.conversion.num).
                success(function (data, status, headers, config) {
                    console.log(data);
                    $scope.conversion.result = data.word;

                }).
                error(function (data, status, headers, config) {
                    console.error(data);
                    $scope.conversion.result = "Invalid Request";
                });
        };
    }]);
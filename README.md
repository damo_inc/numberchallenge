# Intro

Example web app to demonstrate conversion from number to word.

## Instructions

### Install global deps:
install nodejs.
`npm install bower -g`
`npm install nodemon -g`

### Install deps
`npm install` from root
`npm install` from public/app`

### Running this repo
from the root:
`npm start`
OR
`npm test`
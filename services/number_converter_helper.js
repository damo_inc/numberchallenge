/*globals module, require */



var numberConverterHelper = (function () {
    "use strict";
    var _ = require('lodash');
    var words = require('./words');

    function isArgValid(number) {
        return !(!_.isFinite(number) || number % 1 !== 0 || number >= Number.MAX_VALUE);
    }

    function buildHundreds(numArr) {


        return numArr.map(function (item, index, originalArr) {
            var word = '';
            var len = numArr.length;
            if (len === 3) {
                word += index === 0 && item ? words.ONES[item] : '';
                word += index === 1 ? _handleTeen(item, originalArr) : '';
                word += index === 2 && item ? words.ONES[item] || '' : '';
            }
            else if (len === 2) {
                word += index === 0 ? _handleTeen(item, originalArr) : '';
                word += index === 1 && item ? words.ONES[item] || '' : '';
            }
            else if (len === 1) {
                word += words.ONES[item];
            }

            return word;
        });

    }

    function _handleTeen(tens, arr) {
        var lastIndex = arr.length - 1;

        if (tens === 1) {
            var teen = words.ONES[10 + arr[lastIndex]];
            arr[lastIndex] = '';
            return teen;
        }

        return words.TENS[tens];
    }

    function addPostfix(numArr, index, origArr) {
        var last = numArr.length - 1;
        if (numArr[0]) {
            numArr[0] += numArr.length === 3 ? words.POSTFIX[0] : '';
            numArr[last] += origArr.length > 1 ? words.POSTFIX[origArr.length - (index + 1)] : '';

        }
        return numArr.join(' ');
    }

    return {
        isArgValid: isArgValid,
        buildHundreds: buildHundreds,
        addPostfix: addPostfix
    };
})();

module.exports = numberConverterHelper;
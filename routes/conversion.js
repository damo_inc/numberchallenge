var express = require('express');
var router = express.Router();
var numberConverter = require('../services/number_converter')

/* GET users listing. */

router.get('/', function (req, res, next) {
    //res.send('respond with a resource');

    res.status(200);
});


router.get('/:number', function (req, res, next) {
    //res.send('respond with a resource');
    var word = numberConverter.convertToWord(parseInt(req.params.number, 10));
    res.status(200).json({"word": word});
});

module.exports = router;

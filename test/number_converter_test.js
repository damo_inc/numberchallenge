/*jshint strict: false */
/*globals exports, require */


var testCase = require('nodeunit').testCase;
var numberConverter = require('../services/number_converter');
var messages = require('../resources/res_bundle');

exports.numberConverterServiceTest = testCase({
    setUp: function setUp(callback) {
        this.results = {
            ZERO: "zero",
            ONES: "nine",
            TENS: "thirty",
            TENS_WITH_ONES: "twenty one",
            HUNDREDS: "four hundred",
            HUNDREDS_WITH_ONES: "four hundred one",
            HUNDREDS_WITH_TEEN: "four hundred fifteen",
            HUNDREDS_WITH_TENS: "four hundred sixty",
            HUNDREDS_WITH_TENS_ONES: "five hundred sixty two",
            THOUSAND_1: "one thousand",
            BILLION: "two hundred fifty three billion"
        };


        callback();
    },

    tearDown: function tearDown(callback) {
        callback();

    },


    testIsNotDecimal: function (test) {
        var f = function () {
            numberConverter.convertToWord(3.14);
        }
        test.throws(f, Error, "DECIMAL not allowed.");
        test.done();
    },

    testIsNumberType: function (test) {
        var f = function () {
            numberConverter.convertToWord('123aa');
        }
        test.throws(f, Error, "String is not allowed.");
        test.done();
    },

    testIsNotMissing: function (test) {
        var f = function () {
            numberConverter.convertToWord();
        }
        test.throws(f, Error, "Null is not allowed.");
        test.done();
    },

    testIsNotNaN: function (test) {
        var f = function () {
            numberConverter.convertToWord(NaN);
        }
        test.throws(f, Error, "NaN is not allowed.");
        test.done();
    },

    testTooMuch: function (test) {
        var f = function () {
            numberConverter.convertToWord(Number.MAX_VALUE);
        }
        test.throws(f, Error, "Max int value reached.");
        test.done();
    },

    testZero: function (test) {

        test.ok(numberConverter.convertToWord(0) === this.results.ZERO, "zero should\'ve been returned, but wasn't.");
        test.done();
    },

    testTens: function (test) {

        test.ok(numberConverter.convertToWord(30) === this.results.TENS, "thirty should\'ve been returned, but wasn't.");
        test.done();
    },

    testHundredsWithTensOnes: function (test) {
        var convertToWord = numberConverter.convertToWord(562);
        test.ok(convertToWord === this.results.HUNDREDS_WITH_TENS_ONES, "562 should\'ve been returned, but wasn't. Returned: " + convertToWord);
        test.done();
    },

    testHundredsWithTeens: function (test) {
        var convertToWord = numberConverter.convertToWord(415);
        test.ok(convertToWord === this.results.HUNDREDS_WITH_TEEN, "415 should\'ve been returned, but wasn't. Returned: " + convertToWord);
        test.done();
    },
    testHundredsWithTen: function (test) {
        var convertToWord = numberConverter.convertToWord(460);
        test.ok(convertToWord === this.results.HUNDREDS_WITH_TENS, "460 should\'ve been returned, but this was returned:" + convertToWord);
        test.done();
    },
    testThousand: function (test) {
        var convertToWord = numberConverter.convertToWord(1000);
        test.ok(convertToWord === this.results.THOUSAND_1, "1000 should\'ve been returned, but wasn't. returned: " + convertToWord);
        test.done();
    },

    testBillion: function (test) {
        var convertToWord = numberConverter.convertToWord(253000000000);
        test.ok(convertToWord === this.results.BILLION, "253000000000 should\'ve been returned, but wasn't. returned: " + convertToWord);
        test.done();
    }

});





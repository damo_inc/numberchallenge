/*jshint strict: false */
/*globals module*/

var messages = {
    DECIMAL_ERROR: "Ooops, I don\'t do decimals. Only ints please.",
    INVALID_ARGUMENT: "INVALID_ARGUMENT. Missing or wrong number provided."
};

module.exports = messages;